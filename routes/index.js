const express = require("express");
const router = express.Router();
const jwt = require("../utils/jwt");
const middleware = jwt.authenticateToken;
const authController = require("../controller/authController");
const productController = require("../controller/productController");
const cartController = require("../controller/cartController");
const transactionController = require("../controller/transactionController");

module.exports = app => {
  // auth
  router.post("/register", authController.register);
  router.post("/login", authController.login);

  // product
  router.post("/product", middleware, productController.addProduct);
  router.get("/products", middleware, productController.listProduct);
  router.get("/product/:id", middleware, productController.detailProduct);

  // cart
  router.post("/cart/add-to-cart", middleware, cartController.addToCart);
  router.get("/cart/:userId", middleware, cartController.detailCart);

  // cart
  router.post(
    "/transaction/submit",
    middleware,
    transactionController.addTransaction
  );
  router.post(
    "/transaction/change-status/:transactionId",
    middleware,
    transactionController.changeStatus
  );

  app.use("/api/v1", router);
};
