const Validator = require("validatorjs");
const randomize = require("randomatic");
const models = require("../models");

const addTransaction = async (req, res) => {
  try {
    const rules = {
      user_id: "required",
      payment_method: "required",
      total_product: "required",
      total_price: "required"
    };

    const validator = new Validator(req.body, rules);

    if (!validator.check()) {
      return req.output(
        req,
        res,
        { message: "Form invalid.", data: validator.errors.all() },
        "info",
        400
      );
    }

    // check user
    const user = await models.userModel.findOne({
      where: {
        id: req.body.user_id
      }
    });

    if (!user) {
      return req.output(
        req,
        res,
        { error: true, message: "user not found" },
        "error",
        400
      );
    }

    req.body.invoice_number = randomize("Aa0", 10);
    req.body.status = "paid";

    await models.transactionModel.create(req.body);
    await models.cartModel.destroy({
      where: {
        user_id: req.body.user_id
      }
    });

    let objectResponse = await {
      error: false,
      message: "Success",
      info: `Success add transaction`,
      data: req.body
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

const changeStatus = async (req, res) => {
  try {
    const { params, body } = req;

    // check transaction
    const transaction = await models.transactionModel.findOne({
      where: {
        id: params.transactionId
      }
    });

    if (!transaction) {
      return req.output(
        req,
        res,
        { error: true, message: "product not found" },
        "error",
        400
      );
    }

    await models.transactionModel.update(
      {
        status: body.status // paid, cancel
      },
      {
        where: {
          id: params.transactionId
        }
      }
    );

    let objectResponse = await {
      error: false,
      message: "Success update status",
      data: body
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

module.exports = { addTransaction, changeStatus };
