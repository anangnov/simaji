const md5 = require("md5");
const Validator = require("validatorjs");
const models = require("../models");
const jwt = require("../utils/jwt");

const login = async (req, res) => {
  try {
    const rules = {
      email: "required",
      password: "required"
    };

    const validator = new Validator(req.body, rules);

    if (!validator.check()) {
      return req.output(
        req,
        res,
        { message: "Form invalid.", data: validator.errors.all() },
        "info",
        400
      );
    }

    req.body.password = md5(req.body.password);

    const checkUsers = await models.userModel.findOne({
      where: {
        email: req.body.email,
        password: req.body.password
      }
    });

    if (!checkUsers) {
      return req.output(
        req,
        res,
        { error: true, message: "Email or Password not found" },
        "info",
        200
      );
    }

    const token = jwt.generateAccessToken(checkUsers.dataValues);

    let objectResponse = await {
      error: false,
      message: "Success",
      data: {
        user_id: checkUsers.id,
        token: token
      }
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

const register = async (req, res) => {
  try {
    const rules = {
      name: "required",
      email: "required|email",
      phone: "required",
      password: "required|min:6",
      address: "required"
    };

    const validator = new Validator(req.body, rules);

    if (!validator.check()) {
      return req.output(
        req,
        res,
        { message: "Form invalid.", data: validator.errors.all() },
        "info",
        200
      );
    }

    req.body.password = md5(req.body.password);
    req.body.status === 1; // default status active

    await models.userModel.create(req.body);

    let objectResponse = await {
      error: false,
      message: "Success",
      info: `Success register`
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

module.exports = { register, login };
