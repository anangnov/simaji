const Validator = require("validatorjs");
const models = require("../models");

const addProduct = async (req, res) => {
  try {
    const rules = {
      name: "required",
      qty: "required",
      price: "required"
    };

    const validator = new Validator(req.body, rules);

    if (!validator.check()) {
      return req.output(
        req,
        res,
        { message: "Form invalid.", data: validator.errors.all() },
        "info",
        200
      );
    }

    await models.productModel.create(req.body);

    let objectResponse = await {
      error: false,
      message: "Success",
      info: `Success add product`,
      data: req.body
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

const listProduct = async (req, res) => {
  try {
    const products = await models.productModel.findAll({});

    let objectResponse = await {
      error: false,
      message: "Success",
      data: products
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

const detailProduct = async (req, res) => {
  try {
    const { params } = req;

    const product = await models.productModel.findOne({
      where: {
        id: params.id
      }
    });

    let objectResponse = await {
      error: false,
      message: "Success",
      data: product
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

module.exports = { addProduct, listProduct, detailProduct };
