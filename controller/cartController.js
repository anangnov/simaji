const Validator = require("validatorjs");
const models = require("../models");

const addToCart = async (req, res) => {
  try {
    const rules = {
      product_id: "required",
      user_id: "required",
      qty: "required"
    };

    const validator = new Validator(req.body, rules);

    if (!validator.check()) {
      return req.output(
        req,
        res,
        { message: "Form invalid.", data: validator.errors.all() },
        "info",
        200
      );
    }

    // check user
    const user = await models.userModel.findOne({
      where: {
        id: req.body.user_id
      }
    });

    if (!user) {
      return req.output(
        req,
        res,
        { error: true, message: "user not found" },
        "error",
        200
      );
    }

    // check product
    const product = await models.productModel.findOne({
      where: {
        id: req.body.product_id
      }
    });

    if (!product) {
      return req.output(
        req,
        res,
        { error: true, message: "product not found" },
        "error",
        200
      );
    }

    if (product.qty < req.body.qty) {
      return req.output(
        req,
        res,
        { error: true, message: "qty out of stock" },
        "error",
        200
      );
    }

    await models.cartModel.create(req.body);

    let objectResponse = await {
      error: false,
      message: "Success",
      info: `Success add to cart`,
      data: req.body
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

const detailCart = async (req, res) => {
  try {
    const { params } = req;
    const cart = await models.sequelize.query(
      `select a.*, b.name, b.description, b.price from carts as a join products as b on a.product_id = b.id where a.user_id = ${params.userId}`
    );

    const totalPrice = cart[0].reduce((a, b) => {
      return a + Number(b.price);
    }, 0);
    const totalProduct = cart[0].length;

    let objectResponse = await {
      error: false,
      message: "Success",
      data: cart,
      total_price: totalPrice,
      total_product: totalProduct
    };

    return req.output(req, res, objectResponse, "info", 200);
  } catch (err) {
    console.log(err);
    return req.output(
      req,
      res,
      { error: true, message: err.message },
      "error",
      500
    );
  }
};

module.exports = { addToCart, detailCart };
